Enekoron.controllers :article do
  get :index, :map => '/:group' do
    @group = Group.first(:name => params[:group])
    @articles = Article.all(:group => @group)
    render 'article/index'
  end

  get :new, :map => '/:group/new' do
    owner_only
    @group = Group.first(:name => params[:group])
    render 'article/new'
  end

  post :create, :map => '/:group' do
    owner_only
    @group = Group.first(:name => params[:group])
    params[:article][:group] = @group
    Article.create(params[:article])
    redirect url_for(:article, :index, @group.name)
  end

  get :show, :map => '/:group/:id' do
    @group = Group.first(:name => params[:group])
    @article = Article.first(:group => @group, :id => params[:id])
    render 'article/show'
  end

  get :edit, :map => '/:group/edit/:id' do
    owner_only
    @group = Group.first(:name => params[:group])
    @article = Article.first(:group => @group, :id => params[:id])
    render 'article/edit'
  end

  put :update, :map => '/:group/:id' do
    owner_only
    @group = Group.first(:name => params[:group])
    @article = Article.first(:group => @group, :id => params[:id])
    params[:article][:group] = @group
    @article.update(params[:article])
    redirect url_for(:article, :index, @group.name)
  end

  get :ask, :map => '/:group/ask/:id' do
    owner_only
    @group = Group.first(:name => params[:group])
    @article = Article.first(:group => @group, :id => params[:id])
    render 'article/ask'
  end

  delete :delete, :map => '/:group/:id' do
    owner_only
    @group = Group.first(:name => params[:group])
    @article = Article.first(:group => @group, :id => params[:id])
    @article.destroy
    redirect url_for(:article, :index, @group.name)
  end
end
