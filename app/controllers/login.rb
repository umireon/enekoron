Enekoron.controllers :login do
  get :index, :priority => :high do
    redirect url_for(:login, :group)
  end

  get :group, :priority => :high do
    render 'login/group'
  end

  post :group, :priority => :high do
    @group = Group.first(:name => params[:login])
    p @group
    if @group and @group.password == params[:password]
      authenticate @group
      redirect url_for(:article, :index, @group.name)
    else
      redirect url_for(:login, :group)
    end
  end
end
