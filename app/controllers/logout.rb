Enekoron.controllers :logout do
  get :index, :priority => :high do
    session.clear
    redirect url_for(:login, :index)
  end
end
