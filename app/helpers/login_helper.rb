# Helper methods defined here can be accessed in any controller or view in the application

Enekoron.helpers do
  def authenticate(ident)
    case ident
    when Group
      session[:type] = :group
      session[:group_id] = ident.id
      session[:group_name] = ident.name
    end
  end
end
