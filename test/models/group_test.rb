require File.expand_path(File.dirname(__FILE__) + '/../test_config.rb')

describe "Group Model" do
  it 'can construct a new instance' do
    @group = Group.new
    refute_nil @group
  end

  it 'should have irreversible password' do
    @group = Group.new
    @group.password = 'testing123'
    refute @group.password.to_s == 'testing123'
  end

  it 'should be an ordinal group by default' do
    @group = Group.new
    assert @group.type == :group
  end
end
