migration 2, :create_articles do
  up do
    create_table :articles do
      p DateTime.class
      
      column :id, Integer, :serial => true
      column :body, DataMapper::Property::Text
      column :title, String

      column :created_at, DateTime
      column :updated_at, DateTime

      column :group_id, Integer, :not_null => true
    end

    create_index :articles, :group_id
  end

  down do
    drop_table :articles
  end
end
