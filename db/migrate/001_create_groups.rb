migration 1, :create_groups do
  up do
    create_table :groups do
      column :id, Integer, :serial => true
      column :name, String
      column :email, String
      column :type, Integer, :default => 2
      column :password, String, :size => 60

      column :created_at, DateTime
      column :updated_at, DateTime
    end

    create_index :groups, :email, :unique => true
    create_index :groups, :name, :unique => true
  end

  down do
    drop_table :groups
  end
end
