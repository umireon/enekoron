#Helper methods defined here can be accessed in any controller or view in the application

Groups.helpers do
  def identity
    @identity ||= begin
      case session[:type]
      when :group
        group = Group.get(session[:group_id])
        group if group.name == session[:group_name]
      end
    end
  end

  def owner?
    group = Group.first(:name => params[:name])
    group == identity
  end

  def admin?
    case identity
    when Group
      identity.type == :admin
    end
  end

  def owner_only
    redirect Enekoron.url_for(:login, :index) unless owner? or admin?
  end

  def admin_only
    redirect Enekoron.url_for(:login, :index) unless admin?
  end
end
