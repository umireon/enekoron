Groups.controllers do
  before do
    @group = Group.first(:name => params[:name]) if params[:name]
  end

  get :index do
    admin_only
    @groups = Group.all
    render 'index'
  end

  get :signup, :priority => :high do
    admin_only
    render 'signup'
  end

  post :create, :map => '' do
    admin_only
    @group = Group.create(params[:group])
    redirect url_for(:show, params[:group][:name])
  end

  get :show, :map => ':name' do
    owner_only
    render 'show'
  end

  get :edit, :map => ':name/edit' do
    owner_only
    render 'edit'
  end

  put :update, :map => ':name' do
    owner_only
    @group.update(params[:group])
    redirect url_for(:show, @group.name)
  end

  get :ask, :map => ':name/ask' do
    owner_only
    render 'ask'
  end

  delete :delete, :map => ':name' do
    owner_only
    @group.destroy
    redirect url_for(:index)
  end
end
