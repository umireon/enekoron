class Article
  include DataMapper::Resource

  # property <name>, <type>
  property :id,    Serial
  property :body,  Text
  property :title, String

  property :created_at, DateTime
  property :updated_at, DateTime
  
  belongs_to :group
end
