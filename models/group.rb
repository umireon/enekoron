class Group
  include DataMapper::Resource

  # property <name>, <type>
  property :id,       Serial
  property :name,     String, :unique => true
  property :email,    String, :unique => true
  property :type,     Enum[:admin, :group], :default => :group
  property :password, BCryptHash

  property :created_at, DateTime
  property :updated_at, DateTime
  
  has n, :article
end
